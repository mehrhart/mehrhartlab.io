Welcome
========


This following wiki focuses on the two parts of my PhD studies:

+ BeAGLE (Benchmark eA Generator for LEptoproduction) : A Monte Carlo generator for electron ion collisions.
+ eg1-DVCS analysis : Incoherent Deep Virtual Compton Scattering (DVCS) on nitrogen with data from the CLAS experiment eg1 at Jefferson Lab

This will :hoverxref:`show a tooltip <beagle>`

.. toctree::
    :maxdepth: 2
    :caption: BeAGLE

    beagle/beagle.rst
    beagle/singularity.rst
    beagle/development.rst

.. toctree::
    :maxdepth: 2
    :caption: Nitrogen DVCS

    eg1/intro.rst
    eg1/carbon.rst
    eg1/pid.rst


