===========
Development
===========


To Do
=====

+ Add back PYQ_IPTF back to PyQM
+ Add option to PYQ_SUPF for the calculated soft part
+ Investigate DPMJet/Pythia safguard for 4-momentum conservation (temporary fix : set ``PARU(11)=0.01``)

