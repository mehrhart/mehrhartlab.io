************
Introduction
************

.. contents:: Table of Contents

BeAGLE - Benchmark eA Generator for LEptoproduction

BeAGLE, a Fortran program designed as a general purpose eA Monte-Carlo generator, was based on DPMJetHybrid, but has been upgraded to improve the nuclear handling:

+ Allowing a hard collision to occur on a neutron as well as a proton, thereby also conserving charge. In DPMJetHybrid, when a neutron was struck it was replaced with the contents of a Pythia ep collision.
+ Reinstating the Fermi momentum of the struck nucleon (lost in DPMJetHybrid), thereby conserving 4-momentum.
+ Having the target rest frame be the same for the struck nucleon and the nucleus on average
+ Optionally, we also allow for multiple nucleons to participate in the collision in the kinematic (:math:`x`, :math:`Q^2` ) region where nuclear shadowing occurs.

For more details, visit the `BNL wiki page on BeAGLE <https://wiki.bnl.gov/eic/index.php/BeAGLE>`_.


How to get
==========

Source via gitlab
-----------------

.. warning::

    BeAGLE is still under development.


BeAGLE is available here : `<https://gitlab.in2p3.fr/BeAGLE/BeAGLE>`_

.. note::

    BeAGLE is not user-friendly to install (cf. `README.md <https://gitlab.in2p3.fr/BeAGLE/BeAGLE/blob/master/README.md>`_). A simpler way to get BeAGLE is to use `singularity`__.

    __ `Singularity image`_


Singularity image
-----------------

The easiest way to get BeAGLE is to download the following singularity image using the following command :

.. code-block:: none

    $ wget -c https://gitlab.in2p3.fr/BeAGLE/beagle_container/-/jobs/artifacts/master/download\?job\=singularity -o beagle.simg

.. seealso::

    For more details on singularity, check out this :ref:`page <singularity-page>` .

