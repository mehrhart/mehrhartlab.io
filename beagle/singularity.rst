.. _singularity-page:

***********
Singularity
***********

.. contents:: Table of Contents

Singularity allow the user to create and run a software inside a "container", which includes all the required dependencies.


Installation
============

In order to install the newest version of Singularity (3.2), Go (>=1.11.1) needs to be installed first :

There are several ways to install Go, but here is the most common (without package manager) :

1. Download Go :

.. code-block:: none

    $ cd /tmp
    $ wget https://dl.google.com/go/go1.12.5.linux-amd64.tar.gz

2. Extract the archive :

.. code-block:: none

    $ sudo tar -C /usr/local -xzf go1.12.5.linux-amd64.tar.gz

3. Set up your environment for Go :

.. code-block:: none

    $ echo 'export GOPATH=${HOME}/go' >> ~/.bashrc
    $ echo 'export PATH=/usr/local/go/bin:${PATH}:${GOPATH}/bin' >> ~/.bashrc
    $ source ~/.bashrc

The next step is to clone the Singularity repository :

.. code-block:: none

    $ mkdir -p $GOPATH/src/github.com/sylabs
    $ cd $GOPATH/src/github.com/sylabs
    $ git clone https://github.com/sylabs/singularity.git
    $ cd singularity

Finally, compile Singularity as a binary :

.. code-block:: none

    $ cd $GOPATH/src/github.com/sylabs/singularity
    $ ./mconfig
    $ make -C builddir
    $ sudo make -C builddir install


For more details, visit the `Singularity user guide <https://www.sylabs.io/guides/3.2/user-guide/>`_.


Download and run an image
=========================

Running a Singularity image is fairly easy :

1. Download the image :

.. code-block:: none

    $ wget -c https://gitlab.in2p3.fr/BeAGLE/beagle_container/-/jobs/artifacts/master/download\?job\=singularity -o beagle.simg

2. Run the image :

.. code-block:: none

    $ singularity shell beagle.simg
    $ cd /path/to/beagle/run/directory
    $ BeAGLE < input.inp

3. That's it, all the BeAGLE environment is loaded with the image and the software is ready to use.

.. note::

    You can find the repository for the BeAGLE container here : `<https://gitlab.in2p3.fr/BeAGLE/beagle_container>`_


Compile your own image
======================


Since BeAGLE is still under development, and if a new version of BeAGLE is ready, the following command allows the creation of a new image from a definition file :


.. code-block:: none

    $ sudo singularity build BeAGLE.simg BeAGLE.def

.. warning::

    Root privileges (``sudo``) are needed to build a new image

Here is an example of the ``BeAGLE.def`` file :

.. code-block:: none

    Bootstrap: library
    From: ubuntu:18.04
    
    %post
    
        # Update add repository
    
          sed -i '$a deb http://us.archive.ubuntu.com/ubuntu bionic main restricted universe multiverse' /etc/apt/sources.list
          sed -i '$a deb http://us.archive.ubuntu.com/ubuntu bionic-updates main restricted universe multiverse' /etc/apt/sources.list
          sed -i  '1d' /etc/apt/sources.list
          apt -y update
          apt -y upgrade
          apt -y install wget
          apt clean
    
        # Install gfortran
    
          apt -y install gcc-8 g++-8 gfortran-8
          update-alternatives --install /usr/bin/cc cc /usr/bin/gcc-8 30
          update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-8 30
          update-alternatives --install /usr/bin/c++ c++ /usr/bin/g++-8 30
          update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-8 30
          update-alternatives --install /usr/bin/gfortran gfortran /usr/bin/gfortran-8 30
          update-alternatives --config cc
          update-alternatives --config gcc
          update-alternatives --config c++
          update-alternatives --config g++
          update-alternatives --config gfortran
          apt clean
    
        # Install build-essential
    
          apt -y install build-essential git unzip
          apt clean
    
        # Install cernlib
    
          apt -y install cernlib
          apt clean
    
    
        # Install fluka
    
          mkdir -p /tmp/sources
          cd /tmp/sources
          wget https://gitlab.in2p3.fr/BeAGLE/beagle_container/raw/master/sources/fluka2011.2x-linux-gfor64bitAA.tar.gz
          mkdir -p /usr/local/BeAGLE/fluka
          tar zxf fluka2011.2x-linux-gfor64bitAA.tar.gz -C /usr/local/BeAGLE/fluka
          export FLUPRO=/usr/local/BeAGLE/fluka
          export FLUFOR=gfortran
          cd $FLUPRO
          make
    
        # Install lhapdf
    
          cd /tmp/sources
          wget https://gitlab.in2p3.fr/BeAGLE/beagle_container/raw/master/sources/lhapdf-5.9.1.tar.gz
          mkdir -p /tmp/builds
          tar zxf lhapdf-5.9.1.tar.gz -C /tmp/builds
          cd /tmp/builds/lhapdf-5.9.1
          ./configure --disable-pyext --prefix=/usr/local
          make
          make install
          rm -r /usr/local/share/lhapdf
          cd /tmp/sources
          wget https://gitlab.in2p3.fr/BeAGLE/beagle_container/raw/master/sources/lhapdf_set.zip
          unzip lhapdf_set.zip -d /usr/local/share
    
        # Install pythia for rapgap
    
          cd /tmp/sources
          wget https://gitlab.in2p3.fr/BeAGLE/beagle_container/raw/master/sources/pythia6428.tgz
          tar xf pythia6428.tgz -C /usr/local/BeAGLE
          cd /usr/local/BeAGLE/pythia6428
          make -f Makefile-dummy lib
          make -f Makefile-pdflib lib
          make lib
          cp lib/* /usr/local/lib/
    
    
        # Install rapgap
    
          cd /tmp/sources
          wget https://gitlab.in2p3.fr/BeAGLE/beagle_container/raw/master/sources/rapgap-3.303.tar.gz
          tar xf rapgap-3.303.tar.gz -C /tmp/builds
          cd /tmp/builds/rapgap-3.303
          mv /tmp/builds/rapgap-3.303/src/rapgap/pydata.f /tmp/builds/rapgap-3.303/src/rapgap/pydata.f.ori
          cp /usr/local/BeAGLE/pythia6428/pydata.f-save /tmp/builds/rapgap-3.303/src/rapgap/pydata.f
          ./configure --disable-shared --prefix=/usr/local --with-pythia6="/usr/local"  --with-lhapdf="/usr/local"
          sed -i -e 's/manual//g' Makefile  
          make
          make install
    
        # Install BeAGLE
    
            cd /usr/local/BeAGLE
            git clone https://gitlab.in2p3.fr/BeAGLE/BeAGLE.git
            cd BeAGLE
            git checkout BeAGLE_PyQM_beta
            cp /usr/local/lib/libar4.a /usr/local/lib/libbases.a /usr/local/lib/librapgap33.a RAPGAP-3.302/lib/
            cd /usr/local/BeAGLE/BeAGLE/RAPGAP-3.302/lib
            ar d librapgap33.a sfecfe.o
            cd /usr/local/BeAGLE/BeAGLE
            export FLUKA=/usr/local/BeAGLE/fluka/
            export LIB1="-L/usr/lib/x86_64-linux-gnu -lmathlib -lkernlib -lpacklib -ldl -lm"
            export LIB2="-L$FLUKA -lflukahp"
            export LIB3="-L/usr/local/lib -lLHAPDF"
            sed -i '/FLUKA = / s/^/#/' Makefile
            sed -i '/LIB1 = / s/^/#/' Makefile
            sed -i '/LIB2 = / s/^/#/' Makefile
            sed -i '/LIB3 = / s/^/#/' Makefile
            make all
    
    
    %environment
        export LC_ALL=C
    
    %labels
        Author Mathieu Ehrhart

.. note::

    The previous definition file will compile the version of BeAGLE on the ``git`` branch ``BeAGLE_PyQM_beta``. If you need to compile another branch change the line :

    .. code-block:: none

        git checkout BeAGLE_PyQM_beta

    to

    ..  code-block:: none

        git checkout name_of_the_branch



